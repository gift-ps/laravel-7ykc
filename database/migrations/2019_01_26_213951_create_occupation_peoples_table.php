<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationPeoplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('occupation_peoples')){

            Schema::create('occupation_peoples', function (Blueprint $table) {
                $table->index(['occupation_id', 'person_id']);
                
                $table->integer('occupation_id')->unsigned()->nullable();
                $table->integer('person_id')->unsigned()->nullable();

                $table->foreign('occupation_id')->references('id')->on('occupation')->onDelete('cascade');
                $table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');

                $table->increments('id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occupation_peoples');
    }
}
