<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageMytitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('image_mytitles')){

            Schema::create('image_mytitles', function (Blueprint $table) {
                $table->index(['image_id', 'mytitle_id']);
                
                $table->integer('image_id')->unsigned()->nullable();
                $table->integer('mytitle_id')->unsigned()->nullable();

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('mytitle_id')->references('id')->on('mytitles')->onDelete('cascade');

                $table->increments('id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_mytitles');
    }
}
