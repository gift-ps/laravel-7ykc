<?php

namespace App\Admin\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class Movies extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\Models\Movie::count();
        $string = 'Movies';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-tv',
            'title'  => "{$count} {$string}",
            'text'   => __("There are {$count} {$string} in the database. Click on button below to view all {$string}.", 
                            ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('View movies'),
                'link' => route('voyager.movies.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    /*
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', '\App\Models\Movie');
    }
    */
}
