<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Mytitle extends Model
{
    protected $fillable = [
        'id',
        'title',
        'description',
        'country',
        'studio',
        'year',
        'theme',
        'language',
        'producer',
        'director',
        'script',
        'image_id'
    ];

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'mytitle_id');
    }

}
