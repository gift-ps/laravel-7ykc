<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Personsimage extends Model
{
    protected $fillable = [
        'id',
        'name',
        'person_id',
    ];

    /**
     * Relationships
     */
    public function person()
    {
        return $this->belongsTo('App\Models\Person', 'person_id');
    }
}
