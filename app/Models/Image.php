<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    protected $fillable = [
        'id',
        'name',
        'mytitle_id',
    ];

    /**
     * Relationships
     */
    public function mytitle()
    {
        return $this->belongsTo('App\Models\Mytitle', 'mytitle_id');
    }
}
