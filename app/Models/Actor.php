<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    
    public function titles()
    {
        return $this->belongsToMany('App\Models\Title', 'actor_title', 'actor_id', 'title_id');
    }

   public function occupations()
    {
        return $this->belongsToMany('App\Models\Occupation', 'actor_occupation', 'actor_id', 'occupation_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'id');
    }
}
