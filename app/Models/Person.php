<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Person extends Model
{
    protected $fillable = [
        'id',
        'fname',
        'lname',
        'gender',
        'bday',
        'city',
        'bio',
        'occupation_id',
    ];

    public function occupations()
    {
        return $this->belongsToMany('App\Models\Occupation', 'occupation_peoples');
    }
    public function images()
    {
        return $this->hasMany('App\Models\Personsimage', 'person_id');
    }
}
//, 'occupation_peoples', 'person_id', 'occupation_id'