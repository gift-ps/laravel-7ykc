<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{

    protected $fillable = [
        'id',
        'poster',
        'title',
        'description',
        'video',
        'country',
        'studio',
        'year',
        'theme',
        'language',
        'user_id',
        'producer_1',
        'producer_2',
        'actor_1',
        'actor_2',
        'actor_3',
        'actor_4',
        'actor_5',
        'director',
        'script',
        'images'

    ];
/*
    public function actors()
    {
        return $this->belongsToMany('App\Models\Actor', 'actor_movie', 'movie_id', 'actor_id');
    }

    public function themes()
    {
        return $this->belongsToMany('App\Models\Theme');
    }

    public function producers()
    {
        return $this->belongsToMany('App\Models\Producer', 'movie_producer', 'producer_id', 'movie_id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Language');
    }

    public function director()
    {
        return $this->belongsTo('App\Models\Director', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function script()
    {
        return $this->belongsTo('App\Models\Script', 'id');
    }

    public function studio()
    {
        return $this->belongsTo('App\Models\Studio');
    }

    public function year()
    {
        return $this->belongsTo('App\Models\Year');
    }
*/
}
