<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Title extends Model
{

    /**
     * @see https://stackoverflow.com/a/25472319/470749
     * 
     * @param array $arrayOfArrays
     * @return bool
     */
    public static function insertIgnore($arrayOfArrays) {
        $static = new static();
        $table = with(new static)->getTable(); //https://github.com/laravel/framework/issues/1436#issuecomment-28985630
        $questionMarks = '';
        $values = [];
        foreach ($arrayOfArrays as $k => $array) {
            if ($static->timestamps) {
                $now = \Carbon\Carbon::now();
                $arrayOfArrays[$k]['created_at'] = $now;
                $arrayOfArrays[$k]['updated_at'] = $now;
                if ($k > 0) {
                    $questionMarks .= ',';
                }
                $questionMarks .= '(?' . str_repeat(',?', count($array) - 1) . ')';
                $values = array_merge($values, array_values($array));
            }
        }
        $query = 'INSERT IGNORE INTO ' . $table . ' (' . implode(',', array_keys($array)) . ') VALUES ' . $questionMarks;
        return DB::insert($query, $values);
    }

    /**
     * Relationships defined bellow
     */
    public function images()
    {
        return $this->hasMany('App\Models\Appdatum');
    }

    
    public function actors()
    {
        return $this->belongsToMany('App\Models\Actor', 'actor_title', 'title_id', 'actor_id');
    }

    public function themes()
    {
        return $this->belongsToMany('App\Models\Theme');
    }

    public function producers()
    {
        return $this->belongsToMany('App\Models\Producer', 'producer_title', 'producer_id', 'title_id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Language');
    }
     /**
     * Director that made the title/film.
     */
    public function director()
    {
        return $this->belongsTo('App\Models\Director', 'id');
    }
    /**
     * Country that owns the title/film.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    /**
     * Screenwriter that wrote the title/film.
     */
    public function script()
    {
        return $this->belongsTo('App\Models\Script', 'id');
    }

     /**
     * The Studio that made the title/film.
     */
    public function studio()
    {
        return $this->belongsTo('App\Models\Studio');
    }


     /**
     * Year in which title was made.
     */
    public function year()
    {
        return $this->belongsTo('App\Models\Year');
    }
}
