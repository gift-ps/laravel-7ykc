<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Occupation extends Model
{
    protected $fillable = [
        'id',
        'name',
        'occupation',
        'occupation_id',
    ];

    public function people()
    {
        return $this->belongsToMany('App\Models\Person', 'occupation_peoples', 'person_id', 'occupation_id');
    }
}
