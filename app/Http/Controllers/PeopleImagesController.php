<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personsimage;
use App\Models\Person;

class PeopleImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($person)
    {
        return view('people.uploadimg', ['person'=>$person]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //Delete if works -- $personid = $request->person_id;
         //return dd($myid);
        if ($request->hasFile('images'))
        {
            foreach($request->images as $image)
            {
                
                $img_name = $image->getClientOriginalName();    
                $image->storeAs('/public/people/images',$img_name);
                $personid = $request->person_id;

                $images = new Personsimage;
                $images->name       = $img_name;
                $images->person_id = $personid;    
                $images->save();
                // print_r('<hr>'.$imgpath.'<hr>');

            }
            
        }
        $person = Person::where('id','LIKE','%'.$personid.'%')->get();
        //return ($mytitle);
        //$mytitle = json_decode($mytitle, true);
        return redirect()->route('people.show', $personid);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
