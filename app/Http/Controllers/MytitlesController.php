<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Mytitle;

class MytitlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('titles.uploadmytitle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img = $request->poster;
        $img_name = $img->getClientOriginalName();
        $request->poster->storeAs('/public/titles/posters',$img_name);
        //$img_name->store('/public/titles/posters');

        $mytitle = new Mytitle;

            $mytitle->poster   = $img_name;
            $mytitle->name    = $request->input('title');
            $mytitle->theme    = $request->input('theme');
            $mytitle->country  = $request->input('country');
            $mytitle->studio   = $request->input('studio');
            $mytitle->year     = $request->input('year');
            $mytitle->language = $request->input('language');
            $mytitle->director = $request->input('director');
            $mytitle->script   = $request->input('script');
            $mytitle->description = $request->input('description');

            $mytitle->save();

            if($mytitle){
                //return ($mytitle);
                return redirect()->route('uploadimages', ['mytitle_id'=> $mytitle->id]);
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mytitle $mytitle)
    {
        $mytitle = Mytitle::find($mytitle->id);
        //$images = $mytitle->images;
        //return dd($mytitle->images);
        return view('titles.showmytitle', ['mytitle'=>$mytitle]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
