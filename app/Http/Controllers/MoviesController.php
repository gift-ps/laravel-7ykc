<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('movies.index', ['movies' => $movies ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.upload');
    }

    public function embend()
    {
        return view('movies.embend');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        
        $videoFile = $request->video;
        $videoPath = '/Movies/Videos/' . $videoFile;
        $disk = Storage::disk('local');
        $video = $disk->put($videoPath, fopen($videoFile, 'r+'));
        return $video;

        $video = $request->video->save('Movies/Videos');
        return $video;
        
        $poster = $request->poster->store('Movies/Posters');
        return $poster;

        $movie = Movie::create([
                'video' => $request->input('name'),
                'description' => $request->input('description'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),
                'name' => $request->input('name'),

                'user_id' => Auth::user()->id
            ]);

        

    }

    public function storeEmbended(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        $movie = Movie::find($movie->id);
            
            /** Get similar movie by theme - genera */
            $theme = $movie->theme;
                $similar = Movie::where('theme','LIKE','%'.$theme.'%')->get();
                
            /** Get movies by the same studio */
            $studio = $movie->studio;
                $sameStudio = Movie::where('studio','LIKE','%'.$studio.'%')->get();

            return view('movies.show', ['movie'=>$movie,
                                        'similar'=>$similar,
                                        'sameStudio'=>$sameStudio]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
