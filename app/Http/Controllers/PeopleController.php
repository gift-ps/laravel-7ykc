<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Occupation;
use App\Models\Person;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $occupations = Occupation::all();

        return view('people.create', ['occupations'=>$occupations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img = $request->pic;
        $img_name = $img->getClientOriginalName();
        $request->pic->storeAs('/public/people/profile-pics',$img_name);
        $dob = $request['dob-year']."-". $request['dob-month']."-".$request['dob-day'];
        $active = $request['active-year']."-". $request['active-month']."-1";
        //$gig = implode(", ",$_POST["job"]);
   
        //Transform the value from db to an array with = $gig_array = explode(",",$occupation->name);

        $person = new Person;

            $person->pic   = $img_name;
            $person->fname    = $request->input('fname');
            $person->lname    = $request->input('lname');
            $person->gender  = $request->input('gender');
            $person->bday   = $dob;
            $person->city     = $request->input('city');
            $person->bio = $request->input('bio');
           // $person->occupation_id = $gig;
            $person->country = $request->input('country');
            $person->activesince = $active;

            $person->save();

            if($person){
                return redirect()->route('peopleimages.create', ['person'=>$person->id]);
                //'people.uploadimg', ['person_id'=>$person_id]
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        $person = Person::find($person->id);
        //How to bring out specific roles !!!
        // $job = 'occupation_id';
        // $occ = Person::where($job,'LIKE','%'.'actor'.'%')->get();
        // return $occ;

        return view('people.show', ['person'=>$person]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
