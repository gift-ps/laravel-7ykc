<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mytitle;
use App\Models\Image;


class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($mytitle_id)
    {
        return view('titles.uploadimg', ['mytitle_id'=>$mytitle_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $mytitleid = $request->mytitle_id;
         //return dd($myid);
        if ($request->hasFile('images'))
        {
            foreach($request->images as $image)
            {
                
                $img_name = $image->getClientOriginalName();    
                $image->storeAs('/public/titles/images',$img_name);
                $mytitle_id = $request->mytitle_id;

                $images = new Image;
                $images->name       = $img_name;
                $images->mytitle_id = $mytitle_id;    
                $images->save();
                // print_r('<hr>'.$imgpath.'<hr>');

            }
            
        }
        $mytitle = Mytitle::where('id','LIKE','%'.$mytitleid.'%')->get();
        //return ($mytitle);
        //$mytitle = json_decode($mytitle, true);
        return redirect()->route('showmytitle', $mytitleid);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
