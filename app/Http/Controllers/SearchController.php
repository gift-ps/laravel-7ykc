<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Title;

class SearchController extends Controller
{
    public function search()
    {
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w342');
        $query = Input::get ( 'q' );
        
        $search = Title::where('name','LIKE','%'.$query.'%')->get(); //return dd($search);
        if(count($search) > 0)
        return view('search')->withDetails($search)->withQuery($query)->with($imageinfo)
        ->withMessage("Showing results for your query $query");
        else return view ('search')->withMessage('Your query did not match any records.');
    }

    public function usa()
    {
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w342');
        $country = "United States of America";
        
        $search = Title::where('country','LIKE','%'.$country.'%')->get(); //return dd($search);
        if(count($search) > 0)
            return view('search')->withDetails($search)->withQuery($country)->with($imageinfo)
            ->withMessage("Showing Movies from $country");
            else return view ('search')->withMessage("Sorry! There are no movies from $country at the moment.. 
            We are constantly working on adding more titles into our database. Please check back latter. 
            You can also add titles by joining our community");
    }

    public function newZ()
    {
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w342');
        $country = "New Zealand";
        
        $search = Title::where('country','LIKE','%'.$country.'%')->get(); //return dd($search);
            if(count($search) > 0)
            return view('search')->withDetails($search)->withQuery($country)->with($imageinfo)
            ->withMessage("Showing Movies from $country");
            else return view ('search')->withMessage("Sorry! There are no movies from $country at the moment.. 
            We are constantly working on adding more titles into our database. Please check back latter. 
            You can also add titles by joining our community");
    }

    public function uk()
    {
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w342');
        $country = "United Kingdom";
        
        $search = Title::where('country','LIKE','%'.$country.'%')->get(); //return dd($search);
            if(count($search) > 0)
            return view('search')->withDetails($search)->withQuery($country)->with($imageinfo)
            ->withMessage("Showing Movies from $country");
            else return view ('search')->withMessage("Sorry! There are no movies from $country at the moment.. 
            We are constantly working on adding more titles into our database. Please check back latter. 
            You can also add titles by joining our community");
    }

    public function zm()
    {
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w342');
        $country = "Zambia";
        
        $search = Title::where('country','LIKE','%'.$country.'%')->get(); //return dd($search);
            if(count($search) > 0)
            return view('search')->withDetails($search)->withQuery($country)->with($imageinfo)
            ->withMessage("Showing Movies from $country");
            else return view ('search')->withMessage("Sorry! There are no movies from $country at the moment.. 
            We are constantly working on adding more titles into our database. Please check back latter. 
            You can also add titles by joining our community");
    }

}






/**
 * 
 * 
 * $search = Title::where('name', 'LIKE', '%'.$query.'%')->get();
 *       return dd($search);            
 *      return view('search', [
 *         'result' => $search,
 *        'query' => $query
 *       ]);
 * 
 * 
 */