<?php

namespace App\Http\Controllers;

use App\Models\Title;
use App\Models\Mytitle;
use App\Models\Director;
use Illuminate\Http\Request;

class TitlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                            'imgsize' => 'w500');
        $titles = Title::all();
        $mytitles = Mytitle::all();
        return view('titles.index', ['titles' => $titles,'mytitles' => $mytitles])->with($imageinfo);

        /** Available Image sizes  "w92",w154,w185,w342,w500,w780,original,*/                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function save()
    {
        ini_set('max_execution_time', 50);
        /**
         * Saving Dicover Movie Titles
         */
        $page = "8"; $rg = "NG";
        $apikey = "487fab9ddd30a7c5518757cf04cb2f2e";
        $discjson_data = file_get_contents("https://api.themoviedb.org/3/discover/movie?api_key=$apikey&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page");
        $discjson = json_decode($discjson_data,true);
        //echo $filmjson['results']['0']['poster_path'];
        $titlez = array();

        foreach($discjson['results'] as $result){
            $title = [

            'name' => $result['title'],
            'description' => $result['overview'],
            'poster' => $result['poster_path'],
            'tmdb_id' => $result['id'],
            //$genre_id = $result['genre_ids'];
            ];
            
            Title::insertIgnore([$title]);

           // array_push($titlez,$title);

            /**
             * Begin saving movie detailes withing a discover foreach
             */
                //////////////////////////////////////////////////////////////////
                            ini_set('max_execution_time', 300);
                            $titles = Title::all();
                            
                            foreach($titles as $title){

                                        $id = $title->tmdb_id;
                                        $apikey = "487fab9ddd30a7c5518757cf04cb2f2e";
                                        $detailedjson_data = file_get_contents("https://api.themoviedb.org/3/movie/$id?api_key=$apikey&language=en-US");
                                        $detailedjson = json_decode($detailedjson_data, true);
                                        $muviz = array();

                                        

                                    $title = Title::where('tmdb_id', $id)
                                        ->update([
                                            'budget' => $detailedjson['budget'],
                                            'popularity' => $detailedjson['popularity'],
                                            'release_date' => $detailedjson['release_date'],
                                            'revenue' => $detailedjson['revenue'],
                                            'runtime' => $detailedjson['runtime'],
                                            'vote_average' => $detailedjson['vote_average'],
                                            'vote_count' => $detailedjson['vote_count'],
                                            /** Arays down here */
                                            
                                            //'country' => $detailedjson['production_countries']['0']['name'],
                                            //'genres' => $detailedjson['genres']['1']['name'],
                                            //'languages' => $detailedjson['spoken_languages']['0']['name'],
                                            
                                        ]); 

                            } 
        /**
         * Finish saving detailes, and everything really.
         */
        }
        return ('Movies stored succesifully!!!!');

    }
        /**
         * Saving Detailed Movie Titles
         */
    public function saveDetailed()
    {
        ini_set('max_execution_time', 300);
        $titles = Title::all();
        
        foreach($titles as $title){

                    $id = $title->tmdb_id;
                    $apikey = "487fab9ddd30a7c5518757cf04cb2f2e";
                    $detailedjson_data = file_get_contents("https://api.themoviedb.org/3/movie/$id?api_key=$apikey&language=en-US");
                    $detailedjson = json_decode($detailedjson_data, true);
                    $muviz = array();

                $title = Title::where('tmdb_id', $id)
                    ->update([
                        'budget' => $detailedjson['budget'],
                        'popularity' => $detailedjson['popularity'],
                        'release_date' => $detailedjson['release_date'],
                        'revenue' => $detailedjson['revenue'],
                        'runtime' => $detailedjson['runtime'],
                        'vote_average' => $detailedjson['vote_average'],
                        'vote_count' => $detailedjson['vote_count'],
                        /** Arays down here */
                        'studio' => $detailedjson['production_companies']['0']['name'],
                        'country' => $detailedjson['production_countries']['0']['name'],
                        'genres' => $detailedjson['genres']['0']['name'],
                        //'languages' => $detailedjson['spoken_languages']['0']['name'],
                    ]);
        } 
        //return dd($movie);

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Title $title, Mytitle $mytitle)
    {   
       
        /**
         * Decoding cast and showing them directly from TMDb
         */
        $id = $title->tmdb_id;
        $apikey = "487fab9ddd30a7c5518757cf04cb2f2e";
        $detailedjson_data = file_get_contents("https://api.themoviedb.org/3/movie/$id/credits?api_key=$apikey");
        $detailedjson = json_decode($detailedjson_data, true);
        $muviz = array();
        $actors = $detailedjson['cast'];

        $imgbaseurl = 'https://image.tmdb.org/t/p/w342';
        
        foreach($actors as $cast){
            $name = $cast['name'];
            $char = $cast['character'];
            $dp = $cast['profile_path'];
            $id = $cast['id'];
            //echo $cast['name'];
            $casta = $cast;
            
            $imageinfo = array('imgbaseurl' => 'https://image.tmdb.org/t/p/',
                                'imgsize' => 'w342',
                                'name' => $name,
                                'profile_path' => $dp,
                                'cast' => $detailedjson['cast'],);

            $title = Title::find($title->id);
                /** Get Similar movies by genera */
                $genere = $title->genres;
                    $similar = Title::where('genres','LIKE','%'.$genere.'%')->get();
                /** Get movies by the same studio */
                $studio = $title->studio;
                    $sameStudio = Title::where('studio','LIKE','%'.$studio.'%')->get();
                
            $mytitle = Mytitle::find($mytitle->id);

            return view('titles.show', ['title'=>$title,
                                        'similar'=>$similar,
                                        'sameStudio'=>$sameStudio,
                                        'mytitle'=>$mytitle])
                                        ->with($imageinfo);
                                        //->with($actors); //->with($cast);

         }
    } 
    
    /**
     * Show the form for editing the specified resource.
     *  
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
