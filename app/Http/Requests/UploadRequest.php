<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/x-flv',
            'poster' => '',
            'trailer' => 'nullable|mimes:mp4,mov,avi,flv',
            'title' => 'nullable|alpha_num',
            'description' => 'nullable|alpha_dash|alpha_num',
            'theme' => 'nullable|alpha_dash',
            'country' => 'nullable|alpha',
            'studio' => 'nullable|alpha_dash|alpha_num',
            'year' => 'nullable|digits:4',
            'language' => 'nullable|alpha',
            'director' => 'nullable|alpha',
            'script' => 'nullable|alpha',
            'actor_1' => 'nullable|alpha',
            'actor_2' => 'nullable|alpha',
            'actor_3' => 'nullable|alpha',
            'actor_4' => 'nullable|alpha',
            'actor_5' => 'nullable|alpha',
            'producer_1' => 'nullable|alpha',
            'producer_2' => 'nullable|alpha',

        ];
    }
}
