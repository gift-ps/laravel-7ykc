<?php

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'dmback-end'], function () {
    Voyager::routes();
});

Auth::routes();

/**
 * Temp search routes before hooking it up with a dynamic search
 * algorythm
 */
    Route::any('/search', 'SearchController@search')->name('search');
    Route::get('/country/usa', 'SearchController@usa')->name('usa');
    Route::get('/country/zambian', 'SearchController@zm')->name('zm');
    Route::get('/country/british', 'SearchController@uk')->name('uk');
    Route::get('/country/newzealand', 'SearchController@newZ')->name('newz');

/**
 * Resources routes
 */
Route::resources([
    'movies' => 'MoviesController',
    'titles' => 'TitlesController',
    'people' => 'PeopleController',
    'peopleimages' => 'PeopleImagesController'
    ]);

/**
 * Saving titles from TMDB API
 */
    Route::get('/save', 'TitlesController@save')->name('save');
    Route::get('/update', 'TitlesController@saveDetailed')->name('saveDetailed');

/*
 *Fooetr Routes
 */
Route::get('/developer', function() {
    return view('footer.developer');
});

Route::get('/disclaimer', function(){
    return view('footer.legal.disclaimer');
})->name('disclaimer');

/**
 * Upload and store MyTitles and Images
 * 
 */
Route::get('/upload_mytitle', 'MytitlesController@create');
Route::post('/store_mytitle', 'MytitlesController@store')->name('storetitle');
Route::get('/upload_images/{mytitle_id}', 'ImagesController@create')->name('uploadimages');
Route::post('/store_images', 'ImagesController@store')->name('storeimages');
//Show MyTitle
Route::get('/show_mytitle/{mytitle}', 'MytitlesController@show')->name('showmytitle');

//People & People images routes

Route::get('/pip', function() {
    return view('people.index');
});

Route::get('/peopleimgs/{person}', 'PeopleimagesController@create')->name('peopleimages.create');

