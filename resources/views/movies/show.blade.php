@extends('layouts.dm')
<style>
    @media (max-width: 500px){
        .player{
            margin-right: -14%;
            margin-left: -14%;
        }
    }
</style>
@section('main')
<div class="container">
    <div class="row">

        <div class="container-features">
            <div class="container ">
                            
                <div class="container-fluid container-home set-video">
                    <div class="col-md-12 col-md-offset-0">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                        
                            <div class="player">
                                <video class="player__video p-video viewer" src="../storage/movies/elon.mp4" poster="../storage/{{ $movie->poster }}"></video>

                                <div class="player__controls">
                                    <div class="progress">
                                        <div class="progress__filled"></div>
                                    </div>
                                    <button class="player__button play toggle" title="Toggle Play">►</button>
                                    <input type="range" name="volume" class="player__slider" min="0" max="1" step="0.05" value="1" title="monkey">
                                    <input type="range" name="playbackRate" class="player__slider" min="0.5" max="2" step="0.1" value="1" title="monkey">
                                    <button data-skip="-10" class="player__button">« 10s</button>
                                    <button data-skip="25" class="player__button">25s »</button>
                                    <button class="player__fullscreen" title="Toggle fullscreen">↔️</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-center">{{ $movie->title}} @if($movie->year !== '') <small style="color: #fff"> ({{$movie->year}}) @else @endif</small></h4>
                            <div class="movie-plot">
                                <p>
                                {{ $movie->description }}
                                </p>
                            </div>              <br \>
                            <div class="download-btn text-center">
                                <a href="../storage/{{ $movie->video }}" download>
                                    <button class="btn btn-md" style="border-radius: 9px; background-color:rgba(255, 255, 255, 0.52);"><i class="fa fa-download"></i> Download</button>
                                <a>
                            </div>
                        </div>
                        <style>
                            .set-video {
                                padding: 10px;
                            }
                            .middle {
                                transition: .5s ease;
                                opacity: 0;
                                position: absolute;
                                top: 50%;
                                left: 50%;
                                transform: translate(-50%, -50%);
                                -ms-transform: translate(-50%, -50%);
                                text-align: center;
                                }

                            .main-image:hover .pb-video-frame {
                                opacity: 0.3;
                                }

                            .main-image:hover .middle {
                                opacity: 1;
                                }

                            .pb-video-container {
                                padding-top: 20px;
                                background: #bdc3c7;
                                font-family: Lato;
                            }
                        
                            .pb-video {
                                border: 1px solid #e6e6e6;
                                padding: 5px;
                            }
                        
                                .pb-video:hover {
                                    background: #2c3e50;
                                    opacity: 0.8;
                                }
                        
                            .pb-video-frame {
                                transition: width 2s, height 2s;
                            }
                        
                                .pb-video-frame:hover {
                                    height: 300px;
                                    foreground-color: #eee;
                                }
                        
                            .pb-row {
                                margin-bottom: 10px;
                            }
                        </style>
                    </div>
                </div>
                        
                <div class="col-md-12 text-center">   
                        <a href="../storage/{{ $movie->video }}" target="_blank">
                            <h4 class="trailer"> <i class="fa fa-tv"></i> Watch Teaser </h4>
                        </a>
                </div>

            </div>    
        </div>

        <div class="clearfix hidden-xs" style="width:100%; height:60px;"></div>

        <!-- Other info begins here..-->
            <div class="container">
                    <div class="row cast container-home">
                            <div class="text-center show-title-header lead">Additional Infomation</div>
                        <div class="text-center col-md-10 col-md-offset-1">

                            <div class="col-md-3 col-sm-6 col-xm-12">
                            <p><span class="adi-info-lebo"><b>Year : </b></span> <span class="adi-info-lebo text-left">@if($movie->year !== '') {{$movie->year}} @else Nil @endif</span></p>
                            <p><span class="adi-info-lebo"><b>Budget </b></span> <span class="adi-info-lebo text-left"> Unknown </span></p>
                            </div><!-- /.col-lg-3 -->

                            <div class="col-md-3 col-sm-4 col-xs-12">
                            <p><span class="adi-info-lebo"><b>Runtime </b></span> <span class="text-left adi-info-lebo">@if($movie->runtime !== '') {{ $movie->runtime }} @else Nil @endif<small>minutes</small></span></p>
                            <p><span class="adi-info-lebo"><b>Languages </b></span> <span class="text-left adi-info-lebo">@if($movie->language !== '') {{ $movie->language}} @else Nil @endif </span></p>
                            </div><!-- /.col-lg-3 -->
                        
                            <div class="col-md-3 col-sm-6 col-xm-12">
                            <p><span class="adi-info-lebo"><b>Country </b></span> <span class="text-left adi-info-lebo">@if($movie->country !== '') {{ $movie->country }} @else Nil @endif</span></p>
                            <p><span class="adi-info-lebo"><b>Studio </b></span> <span class="text-left adi-info-lebo">@if($movie->studio !== '') {{ $movie->studio}} @else Nil @endif </span></p> 
                            </div><!-- /.col-lg-3 -->
                        
                            <div class="col-md-3 col-sm-6 col-xm-12">
                            <p><span class="adi-info-lebo"><b>Revenue </b></span> <span class="text-left adi-info-lebo"> Unknown </span></p>
                            <p><span class="adi-info-lebo"><b>Genera </b></span> <span class="text-left adi-info-lebo"> @if($movie->theme !== '') {{ $movie->theme }} @else Nil @endif</span></p>
                            </div><!-- /.col-lg-3 -->
                        
                        </div>
                    </div>

                                                <hr class="j-divider"><!-- CAst Div -->
                
                    <div class="row cast container-home">
                                                <div class="text-center Show-title-header lead"> Cast </div>
                        <div class="roll col-md-10 col-md-offset-1">
                        
                            <div class="actor-line">
                                <div class=" col-md-2 col-sm-4 col-xs-4">
                                    <!--a href="https://api.themoviedb.org/3/person/$actor['id']/images?api_key=487fab9ddd30a7c5518757cf04cb2f2e"-->
                                    <img class="img-circle" src=" $imgbaseurl,$imgsize,$actor['profile_path'] " alt=" " width="120" height="120">
                                    <h4> </h4>
                                    </a>
                                    <h5> </h5>
                                </div>
                            </div>
                        
                        </div>
                    </div>


                                                    <hr class="j-divider"><!-- Images -->

                    <div class="row cast container-home">
                                                    <div class="text-center Show-title-header lead">Screen Shots </div>
                        <div class="roll col-md-10 col-md-offset-1">
                        
                            <div class="actor-line">
                                <div class=" col-md-2 col-sm-4 col-xs-4">
                                    <a href="">
                                    <img class="img-circle" src="../storage/$img" alt="" width="120" height="200">
                                    <h4> </h4>
                                    </a>
                                    <h5> </h5>
                                </div>
                            </div>
                        
                        </div> 
                    </div>

                    <hr class="j-divider"><!-- Simillar -->

                    <div class="row cast container-home">
                                            <div class="text-center Show-title-header lead">More Movies Like {{ $movie->title }} </div>
                            <div class="col-md-10 col-md-offset-1">
                                @foreach($similar as $movie)
                                <div class="">
                                    <div class="col-md-3 pb-video main-image actor-line">
                                        <div class="middle text col-md-12">{{ $movie->description }}</div>
                                        <a href="../movies/{{$movie->id}}">
                                            <img class=" pb-video-frame" width="100%" height="230" src="../storage/{{ $movie->poster }}">
                                            <label class="form-control label-warning text-center">{{ $movie->title }}</label>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                    </div>

                    <hr class="j-divider"><!-- Simillar -->
                    
                    @if($movie->studio !== '')

                    <div class="row cast container-home">
                                            <div class="text-center Show-title-header lead">More Movies By {{ $movie->studio }} </div>
                            <div class="col-md-10 col-md-offset-1">
                                @foreach($sameStudio as $movie)
                                <div class="">
                                    <div class="col-md-3 pb-video main-image actor-line">
                                        <div class="middle text col-md-12">{{ $movie->description }}</div>
                                        <a href="../movies/{{$movie->id}}">
                                            <img class=" pb-video-frame" width="100%" height="230" src="../storage/{{ $movie->poster }}">
                                            <label class="form-control label-warning text-center">{{ $movie->title }}</label>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                    </div>
                    @else <div class="text-center Show-title-header lead">Couldn't find more movies by the same studio!</div>
                    @endif
            </div>
        <!--Other info ends here-->
    </div>
</div>
@endsection