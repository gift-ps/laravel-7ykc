@extends('layouts.dm')

    <style>
        .title-offset{
            padding-left: 20px;
        }
        @media (min-width: 992px) {
            .mycol-md-1
            {
                width: 165px!important;
            }
        }
        .lengh-limit{
            display: block;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        label{
            cursor: pointer!important;

        }
    </style>

@section('main')

<div class="container-home">
    <div class="container">
	    <div class="page-header" id="home">
		    <h1 class="text-center text-danger">WELCOME TO DIGITAL MOVIES TV</h1>
			<br />
		    <h2 class="text-center">
            Enjoy unlimited access to the Digital Movies streaming library. 
            Watch full Local and Internatinal indie movies and shorts.
            <br />
            </h2>
	    </div>
		<div class="text-center">
			<a href="#top"><button class="button-more-danger btn btn-md" type="button" style="margin:10px;">COMEDY</button></a>
            <button class="button-more-danger btn btn-md" type="button" style="margin:10px;">ACTION</button>
            <a href="#top"><button class="button-more-danger btn btn-md" type="button" style="margin:10px;">ROMANCE</button></a>
            <a href="#top"><button class="button-more-danger btn btn-md" type="button" style="margin:10px;">THRILLER</button></a>
            <a href="#top"><button class="button-more-danger btn btn-md" type="button" style="margin:10px;">DRAMA</button></a>
            <a href="#top"><button class="button-more-danger btn btn-md" type="button" style="margin:10px;">SHORTS</button></a>
		</div>
    </div>
</div>
<div class="clearfix hidden-xs" style="width:100%; height:60px;"></div>

<div class="container-features">
    <div class="container">
        @foreach($movies as $movie)
        @if($movie !== '')
        <div class="text-center">
            <div class="col-sm-4 col-xs-4 mycol-md-1 main-image"> 
                <a href="./movies/{{ $movie->id }}" class="">
                    <img class="img-rounded title-frame pb-video-frame" title="{{ $movie->title }} ({{ \Carbon\Carbon::parse($movie->year)->format('Y') }})" 
                        alt="{{ $movie->title }}" src="./storage/{!! $movie->poster !!}" style="height: 180px; width: 100%; display: block;"> 
                        
                        <div class="middle text col-md-12"> {{ $movie->description }} </div>

                    <div class="card-body m-b-md">
                        <div class="card-text links">
                            <label class="form-control label-secondary text-center lengh-limit">
                            {{ $movie->title }} {{ \Carbon\Carbon::parse($movie->year)->format('Y') }} 
                            </label>
                        </div>
                    </div>

                </a>
            </div>
        </div>
        @else       @endif
        @endforeach

        <style>
            .middle {
                transition: .5s ease;
                opacity: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                text-align: center;
                }
            .text {
                color: #eee;
                font-size: 16px;
                background-color: #000;
            }
            .main-image:hover .pb-video-frame {
                opacity: 0.75;
                }
                
            .pb-video-container {
                padding-top: 20px;
                background: #bdc3c7;
                font-family: Lato;
            }
        
            .pb-video {
                padding: 5px;
            }
        
                .pb-video:hover {
                    background: #999;
                }
        
            .pb-video-frame {
                transition: width 2s, height 2s;
            }
        
                .pb-video-frame:hover {
                    height: 280px;
                    foreground-color: #eee;
                }
        
            .pb-row {
                margin-bottom: 10px;
            }
        </style>
    </div>
</div>
@endsection