@extends('layouts.gallery')

<title>DM - Upload Title</title>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/readable/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!--select country stuff-->
@section('main')

  <div class="container">
    <div class="row">

      <div class="col-md-offset-3 col-md-6">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
      @endif


                <h2>Add new movie</h2> 
          <hr>
        <form method="post" action="{{ route('storefilm') }}" enctype="multipart/form-data" class="">
                  {{ csrf_field() }}
           

                          <div class="form-group">
                                <label for="video-file">Choose Full Movie<span class="">*</span></label>
                                <input type="file"
                                        id="video-file"
                                        name="video"
                                        
                                        />
                          </div>

                          <div class="form-group">
                                <label for="poster-file">Choose Movie Poster<span class="">*</span></label>
                                <input type="file"
                                        id="poster-file"
                                        name="poster"
                                        
                                        />
                          </div>
                        <hr>
                          <div class="form-group">
                                <label for="images"><small>(optional)</small>
                                    Choose Movie Promotional Images, <small>and screenshots!</small>
                                </label>
                                <input type="file"
                                        id="images"
                                        name="images[]"
                                        multiple="true"
                                        />
                          </div>
                        <hr>
                   <--        <div class="form-group">
                                  <label for="trailer-file">Choose Movie Trailer <small>(optional)</small></label>
                                  <input type="file"
                                        id="trailer-file"
                                        name="trailer"
                                        />
                          </div>
                          

                          <div class="form-group">
                                <label for="company-name">Title<span class="required">*</span></label>
                                <input   placeholder="Movie title"  
                                          id="company-name"
                                          
                                          name="title"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="movie-plot">Description</label>
                                <textarea placeholder="Movie description" 
                                          style="resize: vertical" 
                                          id="movie-plot"
                                          name="description"
                                          rows="3" spellcheck="false"
                                          class="form-control autosize-target text-left">

                                          
                                          </textarea>
                          </div>

                          <div class="form-group">
                                <label for="genera">Theme</label>
                                <input   placeholder="Action, Comedy, Romance, etc."  
                                          id="genera"
                                          name="theme"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="country">Country</label>
                                <input   placeholder="Country"  
                                          id="country"
                                          name="country"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                              <label>Select Country</label><br>

                              <div id="options"
                                    data-input-name="country"
                                    data-selected-country="ZM">
                              </div>
                          </div>

                          <div class="form-group">
                                <label for="studio">Studio</label>
                                <input   placeholder="Action, Comedy, Romance, etc."  
                                          id="studio"
                                          name="studio"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="yr">Year</label>
                                <input   placeholder="Year produced"  
                                          id="yr"
                                          name="year"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="ln">Language</label>
                                <input   placeholder="Language spoken"  
                                          id="ln"
                                          name="language"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="director">Director</label>
                                <input   placeholder="Director"  
                                          id="director"
                                          name="director"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="script">Writer</label>
                                <input   placeholder="Script Writer"  
                                          id="script"
                                          name="script"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="cast-1">Cast <small>Top billed actors</small></label>
                                <input   placeholder="First cast member"  
                                          id="cast-1"
                                          name="actor_1"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                                
                          <div class="form-group">
                                <label for="cast-2">Cast</label>
                                <input   placeholder="Second cast member"  
                                          id="cast-2"
                                          name="actor_2"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="cast-3">Cast</label>
                                <input   placeholder="Third cast member"  
                                          id="cast-3"
                                          name="actor_3"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="cast-4">Cast</label>
                                <input   placeholder="Forth cast member"  
                                          id="cast-4"
                                          name="actor_4"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="cast-5">Cast</label>
                                <input   placeholder="Fith cast member"  
                                          id="cast-5"
                                          name="actor_5"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                                          
                          <div class="form-group">
                                <label for="prod-1">Producer</label>
                                <input   placeholder="Fisrt producer"  
                                          id="prod-1"
                                          name="producer_1"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                  -->
                          <div class="form-group">
                                <label for="prod-2">Producer</label>
                                <input   placeholder="Second producer"  
                                          id="prod-2"
                                          name="producer_2"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <input type="submit" class="col-md-12 btn btn-sm btn-primary"
                                       value="Submit"/>
                          </div>
                          

                          <hr> <br \>
                
        </form>


      </div>
    </div>
  </div>

@endsection
