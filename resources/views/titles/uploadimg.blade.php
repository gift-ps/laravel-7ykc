@extends('layouts.b4')

<title>DM - Upload Title Cont'd</title>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!--select country stuff-->
@section('main')
  <br />
  <div class="container">
    <div class="row justify-content-md-center">

      <div class="col-md-offset-3 col-md-6">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
      @endif

                <h5>Great! Now add some promotional images for your awesome movie to finish uploading!</h5>

        <form method="post" action="{{ route('storeimages') }}" enctype="multipart/form-data" class="">
                  {{ csrf_field() }}


                        <hr>
                          <div class="form-group">
                                <label for="images">
                                    Choose Movie Promotional Images and Screenshots!
                                </label>
                                <input type="file"
                                        id="images"
                                        name="images[]"
                                        multiple="true"
                                        />
                          </div>
                        <hr>

                          <div class="form-group">
                                    <input type="hidden"
                                        name="mytitle_id"
                                        value="{{$mytitle_id}}"
                                        />
                          </div>

                          <div class="form-group">
                                <input type="submit" class="col-md-12 btn btn-sm btn-primary"
                                       value="Submit"/>
                          </div>
                          

                          <hr> <br \>
                
        </form>


      </div>
    </div>
  </div>

@endsection
