@extends('layouts.dm')

    <title>Discover - dm database</title>

        <link href="{{ asset('css/titles.css') }}" rel="stylesheet">
<style>
  /**
  *Custon css code goes here..
  */
    .sidenav {
        height: 100vh !important; /* Full-height: remove this if you want "auto" height */
        width: 160px; /* Set the width of the sidebar */
        position: absolute; /* Fixed Sidebar (stay in place on scroll) */
        z-index: 1; /* Stay on top */
        top: 0; /* Stay at the top */
        right: 0;
        background-color: #343d46;
        color: #FFF;
        padding-top: 20px;
    }
    .sidenav a {
        padding: 6px 8px 6px 16px;
        text-decoration: none;
        color: #c8ccce;
        display: block;
    }
    .main {
        margin-left: 150px; /* Same as the width of the sidebar */
        padding: 0px 10px;
    }
    .lengh-limit{
        display: block;
        width: 100%;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    @media (min-width: 992px) {
        .mycol-md-1
        {
            width: 125px!important;
        }
    }
    @media (max-width: 800px) {
        .colxs{
            padding-right: 0.8px!important;
            padding-left: 0.8px!important;
        }
    }
</style>
@section('main')
<div class="container">
    <main role="main">
                <div class="">
                    <div class="jumbotron text-center jumbo-image">
                        <div class="set-jumbo">
                            <h1>Discover Movies</h1>
                            <p>Discover the latest Movies and TV Shows being reliesed both locally and internationaly. See what movies are 
                            trending and <a href="#">Review and Rate</a> them</p> 
                            <p>You can also watch Free movies and TV Shows <a href="{{ route('movies.index') }}">here.</a>
                            <div class="j-divider"></div>
                            <medium class="by-country">
                                <a href="{{ route('usa') }}">Hollywood</a> &middot; <a href="{{ route('zm') }}">Zambian</a> &middot; <a href="{{ route('usa') }}">Nollywood</a> &middot; 
                                <a href="">Kenyan</a> &middot; <a href="">South African</a> &middot; <a href="">Tanzanyan</a> &middot; 
                                <a href="">German</a> &middot; <a href="">Mexico</a> &middot; <a href="{{ route('newz') }}">New Zealand</a> &middot; 
                                <a href="{{ route('uk') }}">British</a> &middot; <small><a href="" class="text-info text-lg">More &raquo;</a> </small>
                            </medium>
                            <div class="j-divider"></div>
                        </div>
                    </div>
                </div>

        <!-- Side begins here
            <aside class=" col-md-3 col-sm-12 col-xs-12 sidenav side-bg">
                <div style="padding: 15px;">
                    <div class="p-3 mb-3 bg-light rounded">
                        <h4 class="font-italic">Upcoming Events</h4>
                        <ol class="list-unstyled mb-0">
                        <li><a href="#">The Jema Fest</a></li>
                        <li><a href="#">All White Party</a></li>
                        <li><a href="#">Kaya film Festival</a></li>
                        </ol>
                    </div>

                    <div class="p-3">
                        <h4 class="font-italic">Headlines This Month</h4>
                        <ol class="list-unstyled mb-0">
                        <li><a href="#">Comic coast deadlk</a></li>
                        <li><a href="#">Comic coast deadlk</a></li>
                        <li><a href="#">Comic coast deadlk</a></li>
                        </ol>
                    </div>

                    <div class="p-3">
                        <h4 class="font-italic">Movies Around The World</h4>
                        <ol class="list-unstyled">
                        <li><a href="#">Comic coast deadlk</a></li>
                        <li><a href="#">Comic coast deadlk</a></li>
                        </ol>
                    </div>

                    <div class="p-3">
                        <h4 class="font-italic">Elsewhere</h4>
                        <ol class="list-unstyled">
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                        </ol>
                    </div>
                </div>
            </aside>
        Sidebar ends here-->
        <!-- Sidebar for the blo -->

        <div class="">       
        @foreach($titles as $title)
        @if($title !== '')
        <div class="text-center">
            <div class=" panel pster col-sm-3 col-xs-3 mycol-md-1 colxs"> 
                <a href="./titles/{{ $title->id }}" class="">
                    <img class="img-rounded title-frame" alt="{{ $title->name }}" title="{{ $title->name }}" src="{!! $imgbaseurl,$imgsize,$title->poster !!}" style="height: 140px; width: 100%; display: block;"> 
                </a>
                <div class="card-body m-b-md">
                    <div class="card-text links">
                        <span class="title"><a href="#"><strong class="lengh-limit">{{ $title->name }}</strong></span>
                         <div class="lengh-limit">
                            {{ \Carbon\Carbon::parse($title->release_date)->format('Y') }}
                         </div>
                        <div class="btn-group btn-group-xs">
                            <button type="button" class="btn btn-default"><a href="./titles/{{ $title->id }}">View</a></button>
                        </div>
                    </div>
                </div>
                <!-- Lil Xperiment ends here-->
            </div>
        </div>
        @else       @endif
        @endforeach
        </div>
 
        <div class="">       
        @foreach($mytitles as $title)
        @if($title !== '')
        <div class="text-center">
            <div class=" panel pster col-sm-3 col-xs-3 mycol-md-1 colxs"> 
                <a href="./show_mytitle/{{ $title->id}}" class="">
                    <img class="img-rounded title-frame" alt="{{ $title->name }}" title="{{ $title->name }}" src="./storage/titles/posters/{{ $title->poster }}" style="height: 140px; width: 100%; display: block;"> 
                </a>
                <div class="card-body m-b-md">
                    <div class="card-text links">
                        <span class="title"><a href="#"><strong class="lengh-limit">{{ $title->name }}</strong></span>
                         <div class="lengh-limit">
                            {{ \Carbon\Carbon::parse($title->release_date)->format('Y') }}
                         </div>
                        <div class="btn-group btn-group-xs">
                            <button type="button" class="btn btn-default"><a href="./show_mytitle/{{ $title->id}}">View</a></button>
                        </div>
                    </div>
                </div>
                <!-- Lil Xperiment ends here-->
            </div>
        </div>
        @else       @endif
        @endforeach
        </div>
    

    </main>
</div>
@guest

@else
        @if(Auth::User()->role_id == 1)
                <div class="container">
                    <div class="pull-right">
                        <button><a href="{{url('/save')}}">Save</a></button>
                        <button><a href="{{url('/update')}}">Update - with detailes</a></button>
                    </div>
                </div>
        @endif
@endguest
@endsection

