@extends('layouts.dm')

    <title>View {{ $title->name }}</title>

    <link href="{{ asset('css/showtitle.css') }}" rel="stylesheet">
    <style>
    /**
    *Custon css code goes here..
    */
    .setjumbo{
            margin-top: 2px;
            /* margin-left: -60px; */
            color: white;
            background-color: #07bc4c;
            background-attachment: relative;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-position: center center;
            height: 86%;
            width: 100%;
        }
    </style>

@section('main')

<div class="container">
        <div class="setjumbo text-left img-rounded"> <!--jum-->
            <div class="row jumbo-row">
                        <div class="col-md-4 col-xs-12">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-left img-rounded" alt="Thumbnail" style="height: 100%; width: 100%; display: block;" src="{!! $imgbaseurl,$imgsize,$title->poster !!}" data-holder-rendered="true">
                            </div>
                        </div>


                <div class="col-md-7 col-xs-12 xm-color">
                    <div class=" mb-4 box-shadow">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="text-center title-row"><strong class="title">{{ $title->name }}</strong> ({{ \Carbon\Carbon::parse($title->release_date)->format('Y') }})</div>
                        </div>

                        <div class="description col-md-12 col-xs-12">
                            <div class="well well-sm test" style=""> <p> {{ $title->description }} </p> </div>
                            <h4 class="text-center team-header"> </h4>
                        
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>

                                <!-- Trailer-->
                            <div class="media text-center">
                                <div class="media-body">
                                    <iframe width="620" height="400" src="" frameborder="0" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>

        <hr class="j-divider"> <!-- Additional Info -->

        <div class="row cast">
                <div class="text-center show-title-header lead">Additional Infomation</div>
            <div class="text-center">

                <div class="col-md-3 col-sm-6 col-xm-12">
                <p><span class="adi-info-lebo"><b>Year : </b></span> <span class="adi-info-lebo text-left"> {{ \Carbon\Carbon::parse($title->release_date)->format('Y') }}</span></p>
                <p><span class="adi-info-lebo"><b>Budget </b></span> <span class="adi-info-lebo text-left">{{ $title->budget}}</span></p>
                </div><!-- /.col-lg-3 -->

                <div class="col-md-3 col-sm-4 col-xs-12">
                <p><span class="adi-info-lebo"><b>Runtime </b></span> <span class="text-left adi-info-lebo">{{ $title->runtime }} <small>minutes</small></span></p>
                <p><span class="adi-info-lebo"><b>Languages </b></span> <span class="text-left adi-info-lebo">{{ $title->languages }} </span></p>
                </div><!-- /.col-lg-3 -->
            
                <div class="col-md-3 col-sm-6 col-xm-12">
                <p><span class="adi-info-lebo"><b>Country </b></span> <span class="text-left adi-info-lebo">{{ $title->country }}</span></p>
                <p><span class="adi-info-lebo"><b>Studio </b></span> <span class="text-left adi-info-lebo">{{ $title->studio }}</span></p> 
                </div><!-- /.col-lg-3 -->
            
                <div class="col-md-3 col-sm-6 col-xm-12">
                <p><span class="adi-info-lebo"><b>Revenue </b></span> <span class="text-left adi-info-lebo">{{$title->revenue}}</span></p>
                <p><span class="adi-info-lebo"><b>Genera </b></span> <span class="text-left adi-info-lebo"> {{ $title->genres }}</span></p>
                </div><!-- /.col-lg-3 -->
            
            </div>
        </div>

                                    <hr class="j-divider"><!-- CAst Div -->
    
        <div class="row cast">
                                    <div class="text-center Show-title-header lead"> Cast </div>
            <div class="roll">
            @foreach($cast as $actor)
                <div class="actor-line">
                    <div class=" col-md-2 col-sm-4 col-xs-4">
                        <!--a href="https://api.themoviedb.org/3/person/{{$actor['id']}}/images?api_key=487fab9ddd30a7c5518757cf04cb2f2e"-->
                        <img class="img-circle" src="{!! $imgbaseurl,$imgsize,$actor['profile_path'] !!}" alt="{{ $actor['name'] }}" width="120" height="120">
                        <h4 class="lengh-limit">{{ $actor['name'] }}</h4>
                        </a>
                        <h5 class="lengh-limit">As {{ $actor['character'] }} </h5>
                    </div>
                </div>
            @endforeach
            </div>
        </div>


                                        <hr class="j-divider"><!-- Images -->

        <div class="row cast">
                                        <div class="text-center Show-title-header lead">Screen Shots </div>
            <div class="roll">
            
                <div class="actor-line">
                    <div class=" col-md-2 col-sm-4 col-xs-4">
                        <!--a href="https://api.themoviedb.org/3/person/{{$actor['id']}}/images?api_key=487fab9ddd30a7c5518757cf04cb2f2e"-->
                        <img class="img-circle" src="!! $imgbaseurl,$imgsize,$actor['profile_path'] !!" alt=" " width="120" height="200">
                        <h4> </h4>
                        </a>
                        <h5> </h5>
                    </div>
                </div>
            
            </div> 
        </div>

                                        <hr class="j-divider"><!-- Simillar -->
        <div class="row cast">
                                        <div class="text-center Show-title-header lead">More Movies Like {{$title->name}} </div>
                <div class="roll">
                @foreach($similar as $title)
                    <div class="actor-line">
                        <div class="col-lg-3">
                            <a href="../titles/{{$title->id}}">
                                <img class="img-rounded" src="{!! $imgbaseurl,$imgsize,$title->poster !!}" alt="{{$title->name}}" width="144" height="200">
                            </a>
                            <h4><a class="lengh-limit" href="#">{{$title->title}}</a></h4>
                        </div><!-- /.col-lg-3 --> 
                    </div>    
                @endforeach
                </div>
        </div>

                                        <hr class="j-divider"> <!-- Studio -->
        <div class="row cast">
                                        <div class="text-center Show-title-header lead">More Movies by {{$title->studio}}</div>
                <div class="roll">
                @foreach($sameStudio as $title)
                    <div class="actor-line">
                        <div class="col-lg-3">
                            <a href="../titles/{{$title->id}}">
                                <img class="img-rounded" src="{!! $imgbaseurl,$imgsize,$title->poster !!}" alt="{{$title->name}}" width="144" height="200">
                            </a>
                                <h4><a class="lengh-limit" href="#">{{$title->title}}</a></h4>
                        </div>
                    </div>
                @endforeach
                </div>
        </div>

            <hr class="j-divider"> <!-- Studio -->
            
</div>


@endsection
