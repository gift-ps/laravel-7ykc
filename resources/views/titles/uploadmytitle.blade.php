@extends('layouts.b4')

<title>DM - Upload Title</title>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!--select country stuff-->
@section('main')
      <br />
  <div class="container">
    <div class="row justify-content-md-center">

      <div class="col-md-offset-3 col-md-6">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
      @endif


            <h4>Add new movie to database</h4> 
          <hr>
        <form method="post" action="{{ route('storetitle') }}" enctype="multipart/form-data" class="">
                  {{ csrf_field() }}


                          <div class="form-group">
                                <label for="poster-file">Choose Movie Poster<span class="">*</span></label>
                                <input type="file"
                                        id="poster-file"
                                        name="poster"
                                        required
                                        />
                          </div>
                          

                          <div class="form-group">
                                <label for="company-name">Title<span class="required">*</span></label>
                                <input   placeholder="Movie title"  
                                          id="company-name"
                                          required
                                          name="title"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="movie-plot">Description</label>
                                <textarea placeholder="Movie description" 
                                          style="resize: vertical" 
                                          id="movie-plot"
                                          name="description"
                                          rows="4" spellcheck="false"
                                          class="form-control autosize-target text-left">

                                          
                                          </textarea>
                          </div>

                          <div class="form-group">
                                <label for="genera">Theme</label>
                                <input   placeholder="Action, Comedy, Romance, etc."  
                                          id="genera"
                                          name="theme"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="country">Country</label>
                                <input   placeholder="Country"  
                                          id="country"
                                          name="country"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="studio">Studio</label>
                                <input   placeholder="Action, Comedy, Romance, etc."  
                                          id="studio"
                                          name="studio"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>

                          <div class="form-group">
                                <label for="yr">Year</label>
                                <input   placeholder="Year produced"  
                                          id="yr"
                                          name="year"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="ln">Language</label>
                                <input   placeholder="Language spoken"  
                                          id="ln"
                                          name="language"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="director">Director</label>
                                <input   placeholder="Director"  
                                          id="director"
                                          name="director"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                
                          <div class="form-group">
                                <label for="script">Writer</label>
                                <input   placeholder="Script Writer"  
                                          id="script"
                                          name="script"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                          </div>
                

                          <div class="form-group">
                                <input type="submit" class="col-md-12 btn btn-sm btn-primary"
                                       value="Next ..."/>
                          </div>
                          

                          <hr> <br \    >
                
        </form>


      </div>
    </div>
  </div>

@endsection
