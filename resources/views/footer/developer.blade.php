@extends('layouts.dm')
    <title>Hi -I'm Gift PS KAUNDA</title>
    <style>
        .code{
            background-color: #544444;
            color: #fff;
        }
        .emoji{
            font-size: 110%;
        }
        .home-stuff{
            padding-left:25%;
            padding-right:25%;
            padding-top: 5%;
            line-height:2;
            text-align: justify;
        }
        @media (max-width: 800px) {
            .home-stuff{
                padding-right: 5%;
                padding-left: 5%;
            }
        }
        .maillink{
            color: #444;
            background-color:#07bc4c54;
        }
        .maillink:hover{
            background-color:#07bc4cad;
            text-decoration: none;
        }
        p{
            font-size: 112%;
            font-family: Geneva,'Lucida Sans Unicode',  Verdana;
            color:linear-gradient(135deg, #07bc4c 0%,#6aeea5 22%, #5766af 78%,#6aeea5 100%);
        }
    </style>

@section('main')
<div class="container">
    <div class="home-stuff">
        <p>
            Hello! My name is Gift 'PS' Kaunda. I'm a Zambian software developer, screenwriter, African film enthusiast 
            and a proud Christ believer. I love cake -<em>I don't even need no occasion</em>-, big mango, a good hamburger 
            sandwich and a fresh, boiled maize cob <em>(in no particular order)</em>.
        </p>
            <hr>
        <p>
            Apart from food, I LOVE creating stuff. I started off with basic webpages that merely displayed static texts and 
            images using HTML5 and some CSS. Then I wrote my first <code class="code">hello world</code> in Java. Boy that 
            felt good! In my head I was all ready to hack NASA<span class="emoji">🙈</span> or atleast well on my way 
            there<span class="emoji">😎</span>. While still Javing I 
            figured "I might as well try some Android development", and so i did. Then I went back to the web with PHP and 
            JavaScript. The rest is history.
        </p>
            <hr>
        <p>
        As a film lover, I always try to find new movies to entertain myself with. As an Afrocentrist, it makes sense that
        I desire to see more African content -<em>not just Nigerian and Ghanian</em>- on the internet. And I'm sure as hell 
        not the only one. 
        </p>
        <hr>
        <p>
            But what if there was a way to collect as many movies from Zambia, Tanzania, Kenya, Angola, 
            and the rest of Africa, put them in one place for everyone to watch and enjoy anytime, anywhere?
            The thought of bringing every single movie and documentary on the planet, and beyond into just one platform sounds 
            like a dream worth following. And this project to me seems like a pretty good place to start. 
        </p>
        <hr>
        <p>
            So then if you have any content you'd like to share with a legion of fans like me, or you know someone who does, or 
            perhaps you just want to say hi. Do so via<a class="maillink" href="mailto:gkaunda@hotmail.com">
            email.</a> I'll be glad to hear from you.
        </p>
    </div>
</div>

@endsection

