<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <title>{{ config('app.name', 'Gigital Movies') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .nav-image{
            margin-bottom: -30px;
        }
        .foot-style{
          background-color: #343d46;
          padding: 20px;
          margin-top: 0;
          left: 0;
          bottom: 0;
          min-height: 260px;
          width: 100%;
        }
        .main-content{
            height: 75vh;
            }
        .active {
            background-color: red;
            color: red;
            }
        a:hover{
            color: #feddad;
            text-decoration: none;
        }
        #clr{
            color: #feddad;
        }
    </style>


</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="nav-image navbar-brand" href="{{ url('/') }}">
                        <img src="{{Voyager::image('logos/BylK5PfK8vftZNme8jX1SHm2yySINCzSYYWyqf6p.png')}}" style="height: 48px; padding-top: 10px; margin-top: -17px;"/>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                Discover <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('/titles') }}">Browse Movies</a></li>
                                <li><a href="{{ url('/series') }}">Browse TV</a></li>
                                <li><a href="{{ url('/series') }}">Browse Documentaries</a></li>
                                <li><a href="#">People</a></li>
                            </ul>
                        </li>
                        <li class="{{ Request::url()== url('/movies') ? 'active' : '' }}">
                            <a href="{{ url('/movies') }}">Digital Movies TV</a>
                        </li>
                        <li class="{{ Request::url()== url('/#') ? 'active' : '' }}"><a href="#">Contact</a></li>
                        <li class="{{ Request::url()== url('/blog') ? 'active' : ''  }}"><a href="{{ url('/blog') }}">Blog</a></li>
                    
                        <form action="{{ route('search') }}" method="POST" role="search" class="navbar-form navbar-right ">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="Find Movie, Person, TV, etc." style="width: 300px">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Go!</button>
                                </span>
                            </div>
                        </div>
                        </form>
                        
                    </ul>

                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('/home')}}">
                                            Home
                                        </a>
                                    </li>

                                  @if(Auth::user()->role_id==1)
                                    <li>
                                        <a href="{{ url('/admin')}}">
                                            Admin Panel
                                        </a>
                                    </li>
                                  @endif
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div> <!-- class="container"-->
            <div> <!-- class=" row" style="padding-bottom: 20px;"-->
                <div class="main-content">
                    @yield('content')

                        <footer class="footer">
                            <div class="foot-style">
                            <div class=" ">
                                <div class="row text-center"> 
                                    

                                    
                                    <div class="col-md-3 col-sm-6 col-xm-12">
                                        <div class="show-title-header lead" id="clr">Company</a>  </div>
                                        <p><a href=""> About Digital Movies </a></p>
                                        <p><a href=""> What We Do </a></p>
                                        <p><a href=""> Jobs </a> <small id="clr"> We're Hiring</small></p>
                                    </div>
                                
                                    <div class="col-md-3 col-sm-6 col-xm-12">
                                        <div class="show-title-header lead" id="clr"> Community </a></div>
                                        <p><a href="{{route('register')}}"> Join The Community </a></p>
                                        <p><a href=""> Promote Your Movie </a> <small id="clr">Free</small></p>
                                        <p><a href=""> Contribute </a></p>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xm-12">
                                        <div class="show-title-header lead" id="clr"> Sales and Marketing </a></div>
                                        <p><a href="{{route('register')}}"> Advertise </a></p>
                                        <p><a href=""> Buy Movies </a></p>
                                        <p><a href=""> Make Money </a></p>
                                    </div>
                                
                                    <div class="col-md-3 col-sm-6 col-xm-12">
                                        <div class="show-title-header lead" id="clr"> Legal </a></div>
                                        <p><a href="{{ url('/terms') }}"> Terms Of Use </a></p>
                                        <p><a href="{{ url('/privacy') }}"> Privacy </a></p>
                                        <p><a href="{{ route('disclaimer') }}"> Disclaimer </a></p>
                                    </div>

                                </div>
                                
                                <hr class="featurette-divider">
                            
                                <div class="nav text-center">
                                    <span class="text-md" id="clr" style="font-size: 110%"> &copy; 2018 Digital Movies, Ltd. </span>
                                </div>
                            </div>
                            </div>
                        </footer>

                </div>
            </div>
        </div>

    </div> <!--id app-->
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    
</body>
</html>
