<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>DM Promo imgs</title>

    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet" type="text/css">
    <link href="{{ asset('film/biz/css/theme.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
	
    <link rel="stylesheet" type="text/css" href="{{ asset('film/font-awesome/css/font-awesome.min.css') }}" />

    <!-- unite gallery links bellow -->
	<script type='text/javascript' src="{{ asset('unitegallery/js/jquery-11.0.min.js')}}"></script>	
	<script type='text/javascript' src="{{ asset('unitegallery/js/unitegallery.min.js')}}"></script>	
	<link rel='stylesheet' href="{{ asset('unitegallery/css/unite-gallery.css')}}" type='text/css' />
	<script type='text/javascript' src="{{ asset('unitegallery/themes/tiles/ug-theme-tiles.js')}}"></script>
	
</head>

<body>

    <!-- Nav begins here-->
            <nav class="navbar navbar-default navbar-fixed-top" style="height: 20px">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="nav-image navbar-brand" href="{{ url('/') }}">
                            <img src="{{Voyager::image('appdata/December2018/OuZZb6ecOlPmzkU8Umgz.png')}}" style="height: 48px; padding-top: 10px; margin-top: -17px;"/>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->

                        <ul class="nav navbar-nav">
                            <!-- Disable the dropdown for now -
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                        Discover <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('/titles') }}">Browse Movies</a></li>
                                        <li><a href="{{ url('/series') }}">Browse TV</a></li>
                                        <li><a href="{{ url('/series') }}">Browse Documentaries</a></li>
                                        <li><a href="#">People</a></li>
                                    </ul>
                                </li>
                            -->
                            <li><a href="{{ url('/titles') }}">Discover Movies</a></li>
                            <li><a href="{{ url('/movies') }}">Watch Movies</a></li>
                            <!-- Disable contact and blog links for now -
                                <li><a href="#">Contact</a></li>
                                <li><a href="{{ url('/blog') }}">Blog</a></li>
                            -->
                        </ul>

                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                                                    
                            <form action="{{ route('search') }}" method="POST" role="search" class="navbar-form navbar-right ">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" placeholder="Find Movie, Person, TV, etc." style="width: 300px">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default go-btn" type="submit">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            
                            <!-- Authentication Links -->

                            <!-- Disable the login links for now
                                @guest
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu">

                                        @if(Auth::user()->role_id==1)
                                            <li>
                                                <a href="{{ url('/admin')}}">
                                                    Admin Panel
                                                </a>
                                            </li>
                                        @endif
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endguest
                            -->
                        </ul>
                    </div>
                </div>
            </nav>
    <!--Nav ends here-->


@yield('main')	

<div class="clearfix hidden-xs" style="width:100%; height:40px;"></div>

@include('layouts.footer')


</body>
</html>