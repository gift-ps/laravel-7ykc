<footer id="footer" class="clearfix" style="bottom:0; position: relative; width:100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h5>Digital Movies is a product of <a style="color: #07ba4a;font-weight: bolder" href="{{url('/developer')}}">the PS.</a></h5>
                <p><a style="color: #06ba4a" href="mailto:kaundagift@yahoo.com"> Shoot me an email</a></p>
                <p><button class="green-btn btn-md" type="button"><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+260950482937">&nbsp; +260 950 482 937</a></button></p>
                <br \>
            </div>
            <div class="col-md-3 text-center">
                <p class="">
                    <a class="fa fa-twitter footer-socialicon" target="_blank" href="https://twitter.com/"></a>
                    <a class="fa fa-facebook footer-socialicon" target="_blank" href="https://www.facebook.com/"></a>
                    <a class="fa fa-linkedin footer-socialicon" target="_blank" href="https://linkedin.com/in/giftps"></a>
                </p>
                <h5> &copy; 2019 Digital Movies, all rights reserved.</h5>
                <p><a style="color: #07bc4a;" href="{{ route('disclaimer') }}"> Disclaimer </a></p>
            </div>
            
        </div>
    </div>
</footer>