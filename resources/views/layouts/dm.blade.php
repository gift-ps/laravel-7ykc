<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Digital Movies TV</title>
        
	<!-- CSS includes -->
    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet" type="text/css">
    <link href="{{ asset('film/biz/css/theme.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
	
    <link rel="stylesheet" type="text/css" href="{{ asset('film/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{asset('player/style.css') }}">
    @yield('includes')

    <style>
        .go-btn:hover{
            background-color: #07bc4c;
            color: #fff;
        }
        .upload{
            background-color: #4444444c;
            color: white!important;
        }
    </style>

    <!--link href="{{ asset('css/showtitle.css') }}" rel="stylesheet"-->

    <script type="text/javascript" src="{{ asset('film/js/jquery-1.10.2.min.js') }}"></script>

	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

    <!-- Nav begins here-->
            <nav class="navbar navbar-default navbar-fixed-top" style="height: 20px">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="nav-image navbar-brand" href="{{ url('/') }}">
                            <img src="{{Voyager::image('appdata/December2018/OuZZb6ecOlPmzkU8Umgz.png')}}" style="height: 48px; padding-top: 10px; margin-top: -17px;"/>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->

                        <ul class="nav navbar-nav">
                            <!-- Disable the dropdown for now -
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                        Discover <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('/titles') }}">Browse Movies</a></li>
                                        <li><a href="{{ url('/series') }}">Browse TV</a></li>
                                        <li><a href="{{ url('/series') }}">Browse Documentaries</a></li>
                                        <li><a href="#">People</a></li>
                                    </ul>
                                </li>
                            -->
                            <li><a href="{{ url('/titles') }}">Discover Movies</a></li>
                            <li><a href="{{ url('/movies') }}">Watch Movies</a></li>
                            <!-- Disable contact and blog links for now -
                                <li><a href="#">Contact</a></li>
                                <li><a href="{{ url('/blog') }}">Blog</a></li>
                            -->
                        </ul>

                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                        <li class="upload"><a href="{{ url('/upload_mytitle') }}">Upload Title</a></li>
                                                    
                            <form action="{{ route('search') }}" method="POST" role="search" class="navbar-form navbar-right ">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" placeholder="Find Movie, Person, TV, etc." style="width: 300px">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default go-btn" type="submit">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            
                            <!-- Authentication Links -->

                            <!-- Disable the login links for now
                                @guest
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu">

                                        @if(Auth::user()->role_id==1)
                                            <li>
                                                <a href="{{ url('/admin')}}">
                                                    Admin Panel
                                                </a>
                                            </li>
                                        @endif
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endguest
                            -->
                        </ul>
                    </div>
                </div>
            </nav>
    <!--Nav ends here-->

    <br \>
    <div style="min-height: 70vh;">
        @yield('main')
    </div>

    <div class="clearfix hidden-xs" style="width:100%; height:40px;"></div>

    @include('layouts.footer')

    <!-- Java script -->
        <script src="{{ asset('player/scripts.js') }}"></script>

        <script src="{{ asset('js/app.js') }}"></script>

        <script src="js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <link href="css/magnific-popup.css" rel="stylesheet">
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script src="js/theme.js"></script>
        <script type="text/javascript" src="http://prepbootstrap.com/Content/shieldui-lite/dist/js/shieldui-lite-all.min.js"></script>



        <script type="text/javascript">
            jQuery(function($) {
                // Mix It Up Gallery and Magnific Popup setup
                $('.container-gallery').mixItUp();
                $('.container-gallery').magnificPopup({
                    delegate: 'a',
                    type: 'image'
                });
                // Google Maps setup
                var googlemap = new google.maps.Map(
                    document.getElementById('googlemap'),
                    {
                        center: new google.maps.LatLng(44.5403, -78.5463),
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                );
            });

            $(document).ready(function () {
                $("#order1").shieldQRcode({
                    mode: "byte",
                    size: 150,
                    value: "https://somesite/order?plan=1"
                });
                $("#order2").shieldQRcode({
                    mode: "byte",
                    size: 150,
                    value: "https://somesite/order?plan=2"
                });
                $("#order3").shieldQRcode({
                    mode: "byte",
                    size: 150,
                    value: "https://somesite/order?plan=3"
                });
            });
        </script>
    <!-- JS ends here-->

</body>
</html>
