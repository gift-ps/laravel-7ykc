@extends('layouts/dm')

<style>
    .pster{
        background-color: #ddd;
        padding: 3px;
        padding-bottom: 8px;
       /*margin-left:33px;*/
        }
        .lengh-limit{
        display: block;
        width: 100%;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    @media (min-width: 992px) {
        .mycol-md-1
        {
            width: 125px!important;
        }
    }
    @media (max-width: 800px) {
        .colxs{
            padding-right: 0.8px!important;
            padding-left: 0.8px!important;
        }
    }
</style>    

@section('main')

<div class="container">
        @if(isset($details))
        <div class="text-center">
            <h3>{{$message, $query }}</h3>
        </div>  <br \>
    
        @foreach($details as $title)
        <div class="pster mycol-md-1 colxs col-sm-3 col-xs-3 panel"> 
            <a href="./titles/{{ $title->id }}" class="">
                <img class="panel title-frame" alt="{{ $title->name }}" title="{{ $title->name }}" src="{!! $imgbaseurl,$imgsize,$title->poster !!}" style="height: 140px; width: 100%; display: block;"> 
                
                <div class="card-body m-b-md">
                    <div class="card-text links">
                        <span class="title lead lengh-limit">{{ $title->name }}</span>
                        <div class="lengh-limit">
                        {{ \Carbon\Carbon::parse($title->release_date)->format('Y') }}
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach

        @elseif(isset($message))
            <div class="text-center">
                <h4>{{ $message }}</h4>
                <h5><a href="{{ url('/titles') }}">Go back to the browse movies home page </a></h5>
            </div>
        @endif

</div>
@endsection