<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome - D M</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--My CSS-->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 24px;
                color: #2e4da2;
            }
            a:hover{
                color: #78ad7f !important;
            }
            .auth-links > a {
                color: #26554d;
                padding: 0 25px;
                font-size: 15px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a {
                color: #26554d;
                padding: 0 25px;
                font-size: 22px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .home_bg-1 {
                background-color: #ffffff;
                background-attachment: absolute;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                background-position: center center;
                height: 100%;
                width: 100%;
            }

            .home_bg-2 {
                background-image: linear-gradient(#ff010140, rgba(9, 255, 0, 0.1)), url("{{ Voyager::image('appdata/December2018/UMrEhjwtVtgeEbxY5kUR.jpg') }}" );
                background-attachment: absolute;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                background-position: center center;
                height: 100%;
                width: 100%;
            }

            .m-b-md {
                margin-bottom: 70px;
            }
            .home-txt{
                width: 600px;
                color: #eee;
                background-color: #07bc4cb4;
                border-radius: 500px;
                padding: 10%;
                margin-left: 25%;
            }
            .txt{
                font-size:24px;
            }
            @media (max-width: 1000px) {
                    .hidden-sm {
                        display: none !important;
                    }
                }    
        </style>
    </head>
    <body class="">
      <div class="home_bg-1">
        <div class="flex-center position-ref full-height">
                <!--
                    Down here is the login links that we dont need at the moment
                    
                    @if (Route::has('login'))
                        <div class="top-right auth-links">
                            @auth
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                                <a href="{{ route('login') }}">Login</a>
                                <a href="{{ route('register') }}">Register</a>
                            @endauth
                        </div>
                    @endif
                -->

            <div class="content container">
                <div class="title m-b-md">    
                    <img src="{{Voyager::image('appdata/December2018/pD4DhT4QLdKdqeA3RIOI.png') }}" />
                </div>


                <div class="text-center col-md-12 col-sm-12 col-xm-12">
                  <div class="links col-sm-12 col-xm-12">
                    <a href="{{ url('/titles') }}">Discover</a>
                    <a href="{{ url('/movies')}}">Watch</a>
                    <a href="{{ url('/blog') }}" class="col-xm-12 ">Blog</a>
                  </div>
                </div>

            </div>
        </div>
      </div>

    <!-- Second page begines here -->

        <div class="home_bg-2 text-center">
            <div class="flex-center position-ref full-height">

                <div class="content container text-center">
                    <div class="hidden-sm col-md-12 col-sm-12 col-xm-6">
                        <div class="home-txt">
                            <span class="">
                                <p class="txt text-center"><b>Discover thousands of new movies to enjoy with friends</b>
                                    Kiss boredom goodbye! Keep the loughs going with Brand New Comedies from all over 
                                    Africa and beyond.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>

    </body>
</html>
